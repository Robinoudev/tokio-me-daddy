use mini_redis::{client, Result};

#[tokio::main]
async fn main() -> Result<()> {
    // connect to redis
    let mut client = client::connect("127.0.0.1:6379").await?;

    // set key hello with value world in redis store
    client.set("hello", "world".into()).await?;

    // get value of key hello
    let result = client.get("hello").await?;

    println!("got value from the server; result={:?}", result);

    Ok(())
}
